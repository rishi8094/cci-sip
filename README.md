# CCI SIP

Useful links:
- [Blog]("https://cci.rishis.xyz")
- [SIP Log]("https://cci.rishis.xyz/pages/term2/sip/")
- [GIT]("https://gitlab.com/rishi8094/cci-sip")
- [WEBSITE GIT]("https://gitlab.com/rishi8094/cci.rishis.xyz")

## Summary
 - [Money Talks](#moneytalks)
 - [Morph](#morph)
 - [Prism](#prism)
 - [Ripple](#ripple)
 - [Snake](#snake)


### Money Talks
![](./assets/cash.png)
Money Talks is an exploration of the skills taught in **Week 3, 27 Feb: Audio & Week 6, 16 April**, this sketch takes a deep dive into image filtering as well as sound playback.

- [SKETCH](https://cci.rishis.xyz/pages/term2/sip/final/moneytalks/)
- [GIST](https://gitlab.com/snippets/1985533)


### Morph
![](assets/morph.png)
Morph is a generative patterns made using the skills taught in **Week 2, 20 Feb: Geometry**, this sketch takes a deep dive into the sin/cos formulas as well as an understanding of amplitude radians to create the morph effect.

- [SKETCH](https://cci.rishis.xyz/pages/term2/sip/final/morph/)
- [GIST](https://gitlab.com/snippets/1985502)

### Prism
![](assets/prism.png)
PRISM is an exploration of the skills taught in *Week 1, 13 Feb: Image representation**, this sketch takes a deep dive into the colour theory as well as an understanding of loops an iteration.

- [SKETCH](https://cci.rishis.xyz/pages/term2/sip/final/prism/)
- [GIST](https://gitlab.com/snippets/1985506)

### Ripple
![](assets/spinner.png)

P5.js is used to create the ripple using the default canvas. A circle is created and redrawn with the diameter expanding at a constant rate giving the growing illusion.

The circle is redrawn 250 times with the diameter from the center expanding at a fixed rate to create the illusion of an outward ripple. A map function is used to create the colour stroke effect to create the light to dark ring fade. The circle is then filled with a colour that will adjust itself on a loop. At a certain point the rings are reset to prevent the animation causing slowdowns dues to the colour gradient.

- [SKETCH](https://cci.rishis.xyz/pages/term2/sip/final/ripple/)
- [GIST](https://gitlab.com/snippets/1983944)


### Snake
![](assets/snake.png)

Colourful Snake is an exploration of the skills taught in **Week 4, 5 Mar: Programming techniques**, this sketch takes a deep dive into trails, repetition, sequencing as well as expanding on the knowledge from colour theory in prior weeks.

- [SKETCH](https://cci.rishis.xyz/pages/term2/sip/final/snake/)
- [GIST](https://gitlab.com/snippets/1983878)
